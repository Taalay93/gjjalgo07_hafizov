package com.getjavajob.training.algo07.util;

import java.util.Arrays;

/**
 * Created by admin on 14.07.2015.
 */
public class Assert extends Throwable {
    public static void assertEquals(boolean expected, boolean actual) {
        if (expected == actual) {
            System.out.println("Success");
        } else {
            System.out.println("Failure: expected " + expected + ", actual " + actual);
        }
    }

    public static void assertEquals(String expected, String actual) {
        if (expected.equals(actual)) {
            System.out.println("Success");
        } else {
            System.out.println("Failure: expected " + expected + ", actual " + actual);
        }
    }

    public static void assertEquals(String[] expected, String[] actual) {
        if (Arrays.equals(expected,actual)) {
            System.out.println("Success");
        } else {
            System.out.println("Failure: expected " + Arrays.toString(expected) + ", actual " + Arrays.toString(actual));
        }
    }

    public static void assertEquals(int expected, int actual) {
        if (expected==actual) {
            System.out.println("Success");
        } else {
            System.out.println("Failure: expected " + expected + ", actual " + actual);
        }
    }
    public static void assertEquals(double expected, double actual) {
        if (expected==actual) {
            System.out.println("Success");
        } else {
            System.out.println("Failure: expected " + expected + ", actual " + actual);
        }
    }

    public static void assertEquals(long expected, long actual) {
        if (expected==actual) {
            System.out.println("Success");
        } else {
            System.out.println("Failure: expected " + expected + ", actual " + actual);
        }
    }

    public static void assertEquals(int[] expected, int[] actual) {
        if (Arrays.equals(expected,actual)) {
            System.out.println("Success");
        } else {
            System.out.println("Failure: expected " + Arrays.toString(expected) + ", actual " + Arrays.toString(actual));
        }
    }

    public static void assertEquals(Integer[] expected, Integer[] actual) {
        if (Arrays.equals(expected,actual)) {
            System.out.println("Success");
        } else {
            System.out.println("Failure: expected " + Arrays.toString(expected) + ", actual " + Arrays.toString(actual));
        }
    }

    public static void assertEquals(int[][] expected, int[][] actual) {
        if (Arrays.deepEquals(expected,actual)) {
            System.out.println("Success");
        } else {
            System.out.println("Failure: expected " + Arrays.deepToString(expected) + ", actual " + Arrays.deepToString(actual));
        }
    }

        public static void fail(String msg){
            throw new java.lang.AssertionError(msg);
    }
}