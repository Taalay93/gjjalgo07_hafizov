package com.getjavajob.training.algo07.util;
//import com.getjavajob.training.algo07.hafizov.*;
import com.getjavajob.training.algo07.hafizov.lesson03.DynamicArray;
import com.getjavajob.training.algo07.hafizov.lesson04.DoublyLinkedList;
import com.getjavajob.training.algo07.hafizov.lesson06.AssociativeArray;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;

/**
 * Created by taala on 22.08.2015.
 */
public class PerformanceTest {
    public static String test(LinkedList ll, int q, Object obj,String nameClass) {
        final int begin = q;
        final int middle = begin * 2;
        final int end = begin * 3;
        long addTime, removeTime;
        String result = "";
        StopWatch addLL = new StopWatch();
        addLL.start();
        for (int i = 0; i < begin; i++) {
            ll.add(obj);
        }
        for (int i = begin; i < middle; i++) {
            ll.add(i, obj);
        }
        for (int i = middle; i < end; i++) {
            ll.add(i,obj);
        }
        addLL.stop();
        addTime = addLL.getElapsedTime();

        StopWatch removeLL = new StopWatch();
        removeLL.start();//remove (begin/middle/end)
        for (int i = end-1; i > middle; i--) {
            ll.remove(i);
        }

        for (int i = middle; i > begin; i--) {
            ll.remove(i);
        }

        for (int i = begin; i > 0; i--) {
            ll.remove(obj);
        }
        removeLL.stop();
        removeTime= removeLL.getElapsedTime();

        return nameClass +": add time is "+addTime+"ms \n"+nameClass+": remove time is "+removeTime+"ms \n";
    }

    public static String test(DoublyLinkedList ll, int q, Object obj,String nameClass) {
        final int begin = q;
        final int middle = begin * 2;
        final int end = begin * 3;
        long addTime, removeTime;
        String result = "";
        StopWatch addLL = new StopWatch();
        addLL.start();
        for (int i = 0; i < begin; i++) {
            ll.add(obj);
        }
        for (int i = begin; i < middle; i++) {
            ll.add(i, obj);
        }
        for (int i = middle; i < end; i++) {
            ll.add(i,obj);
        }
        addLL.stop();
        addTime = addLL.getElapsedTime();

        StopWatch removeLL = new StopWatch();
        removeLL.start();//remove (begin/middle/end)
        for (int i = end; i > middle; i--) {
            ll.remove(i);
        }

        for (int i = middle; i > begin; i--) {
            ll.remove(i);
        }

        for (int i = begin; i > 1; i--) {
            ll.remove(obj);
        }
        removeLL.stop();
        removeTime= removeLL.getElapsedTime();

        return nameClass +": add time is "+addTime+"ms \n"+nameClass+": remove time is "+removeTime+"ms \n";
    }

    public static String test(ArrayList ll, int q, Object obj,String nameClass) {
        final int begin = q;
        final int middle = begin * 2;
        final int end = begin * 3;
        long addTime, removeTime;
        String result = "";
        StopWatch addLL = new StopWatch();
        addLL.start();
        for (int i = 0; i < begin; i++) {
            ll.add(obj);
        }
        for (int i = begin; i < middle; i++) {
            ll.add(i, obj);
        }
        for (int i = middle; i < end; i++) {
            ll.add(i,obj);
        }
        addLL.stop();
        addTime = addLL.getElapsedTime();

        StopWatch removeLL = new StopWatch();
        removeLL.start();//remove (begin/middle/end)
        for (int i = end-1; i > middle; i--) {
            ll.remove(i);
        }

        for (int i = middle; i > begin; i--) {
            ll.remove(i);
        }

        for (int i = begin; i > 0; i--) {
            ll.remove(obj);
        }
        removeLL.stop();
        removeTime= removeLL.getElapsedTime();

        return nameClass +": add time is "+addTime+"ms \n"+nameClass+": remove time is "+removeTime+"ms \n";
    }

    public static String test(DynamicArray ll, int q, Object obj,String nameClass) {
        final int begin = q;
        final int middle = begin * 2;
        final int end = begin * 3;
        long addTime, removeTime;
        String result = "";
        StopWatch addLL = new StopWatch();
        addLL.start();
        for (int i = 0; i < begin; i++) {
            ll.add(obj);
        }
        for (int i = begin; i < middle; i++) {
            ll.add(i, obj);
        }
        for (int i = middle; i < end; i++) {
            ll.add(i,obj);
        }
        addLL.stop();
        addTime = addLL.getElapsedTime();

        StopWatch removeLL = new StopWatch();
        removeLL.start();//remove (begin/middle/end)
        for (int i = end-1; i > middle; i--) {
            ll.remove(i);
        }

        for (int i = middle; i > begin; i--) {
            ll.remove(i);
        }

        for (int i = begin; i > 0; i--) {
            ll.remove(obj);
        }
        removeLL.stop();
        removeTime= removeLL.getElapsedTime();

        return nameClass +": add time is "+addTime+"ms \n"+nameClass+": remove time is "+removeTime+"ms \n";
    }

    public static String test(AssociativeArray<String,String> ll, int q, String obj,String nameClass) {
        final int begin = q;
        final int middle = begin * 2;
        final int end = begin * 3;
        long addTime, removeTime, getTime;
        String result = "";

        StopWatch addLL = new StopWatch();
        addLL.start();
        for (int i = 0; i < end; i++) {
            ll.add(obj+i,obj);
        }
        addLL.stop();
        addTime = addLL.getElapsedTime();

        StopWatch getLL = new StopWatch();
        getLL.start();
        for (int i = 0; i < end; i++) {
            ll.get(obj+i);
        }
        getLL.stop();
        getTime= getLL.getElapsedTime();

        StopWatch removeLL = new StopWatch();
        removeLL.start();
        for (int i = 0; i < end; i++) {
            ll.remove(obj+i);
        }
        removeLL.stop();
        removeTime= removeLL.getElapsedTime();

        return nameClass +": add time is "+addTime+"ms \n"+nameClass+": remove time is "+removeTime+"ms \n"+nameClass+": get time is "+getTime+"ms \n";
    }

    public static String test(HashMap<String,String> ll, int q, String obj,String nameClass) {
        final int begin = q;
        final int middle = begin * 2;
        final int end = begin * 3;
        long addTime, removeTime, getTime;
        String result = "";

        StopWatch addLL = new StopWatch();
        addLL.start();
        for (int i = 0; i < end; i++) {
            ll.put(obj+i,obj);
        }
        addLL.stop();
        addTime = addLL.getElapsedTime();

        StopWatch getLL = new StopWatch();
        getLL.start();
        for (int i = 0; i < end; i++) {
            ll.get(obj+i);
        }
        getLL.stop();
        getTime= getLL.getElapsedTime();

        StopWatch removeLL = new StopWatch();
        removeLL.start();
        for (int i = 0; i < end; i++) {
            ll.remove(obj+i);
        }
        removeLL.stop();
        removeTime= removeLL.getElapsedTime();

        return nameClass +": add time is "+addTime+"ms \n"+nameClass+": remove time is "+removeTime+"ms \n"+nameClass+": get time is "+getTime+"ms \n";
    }

    public static void writeFile(String str, String name) {
        try {
            OutputStream f = new FileOutputStream(name);
            OutputStreamWriter writer = new OutputStreamWriter(f);
            BufferedWriter out = new BufferedWriter(writer);
            out.write(str);
            out.flush();
            out.close();
        } catch (IOException ex) {
            System.err.println(ex);
        }
    }
}
