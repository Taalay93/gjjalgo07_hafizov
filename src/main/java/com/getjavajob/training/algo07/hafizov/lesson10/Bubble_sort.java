package com.getjavajob.training.algo07.hafizov.lesson10;

/**
 * Created by taala on 19.09.2015.
 */
public class Bubble_sort<E extends Comparable<E>> {
    public E[] sort(E[] arr) {
        boolean swapped = true;
        int j = 0;
        E tmp;
        while (swapped) {
            swapped = false;
            j++;
            for (int i = 0; i < arr.length - j; i++) {
                if (arr[i].compareTo(arr[i + 1]) > 0 ) {
                    tmp = arr[i];
                    arr[i] = arr[i + 1];
                    arr[i + 1] = tmp;
                    swapped = true;
                }
            }
        }
        return arr;
    }

}
