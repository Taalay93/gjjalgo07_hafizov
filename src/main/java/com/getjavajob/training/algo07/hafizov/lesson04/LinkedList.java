package com.getjavajob.training.algo07.hafizov.lesson04;

/**
 * Created by taala on 23.08.2015.
 */
public class LinkedList<V> {
    Node<V> head, last;

    public void reverse() {
        Node<V> current, next, prev;
        current = head;
        prev = null;

        while (current != null) {
            next = current.next;
            current.next = prev;
            prev = current;
            current = next;
        }
        head = prev;
    }

    public boolean add(V val) {
        Node<V> newLink = new Node<>(val);

        if (isEmpty()) {
            head = newLink;
            last = head;
        } else {
            last.next = newLink;
            last = newLink;
        }
        return true;
    }

    public void insertFirst(V val) {
        Node<V> n = new Node(val);
        n.next = head;
        head = n;
    }

    public V deleteFirst() {
        Node<V> temp = head;
        head = head.next;
        return temp.val;
    }

    public V deleteLast(){
        Node<V> temp = head;
        Node<V> prev = head;
        while (temp.next != null){
            prev = temp;
            temp = temp.next;
        }
        prev.next = null;
        return temp.val;
    }

    public String show() {
        Node<V> pr = head;
        String result = "";
        while (pr.next != null) {
            System.out.println(pr.val);
            result += pr.val+" ";
            pr = pr.next;
        }
        System.out.println(pr.val);
        result += pr.val+" ";
        return result;
    }

    public boolean isEmpty() {
        return head == null;
    }
}
