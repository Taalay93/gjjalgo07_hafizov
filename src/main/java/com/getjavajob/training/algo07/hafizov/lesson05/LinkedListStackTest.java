package com.getjavajob.training.algo07.hafizov.lesson05;
import com.getjavajob.training.algo07.util.*;
/**
 * Created by taala on 23.08.2015.
 */
public class    LinkedListStackTest {

    public static void main(String[] args) {
        test();
    }

    public static void test(){
        LinkedListStack<String> str = new LinkedListStack<>();
        str.push("N50");
        str.push("N175");
        str.push("N80");

        Assert.assertEquals("N80 N175 N50 ", str.displayStack());

        Assert.assertEquals("N80",str.pop());

        Assert.assertEquals("N175 N50 ",str.displayStack());
    }
}
