package com.getjavajob.training.algo07.hafizov.lesson10;

/**
 * Created by taala on 19.09.2015.
 */
public class Merge_sort<E extends Comparable<E>> {

    public void sort(E[] arr) {
        sort(arr, 0, arr.length - 1);
    }

    private void DoMerge(E[] numbers, int left, int mid, int right) {
        E[] temp = (E[]) new Comparable[25];
        int i, left_end, num_elements, tmp_pos;

        left_end = (mid - 1);
        tmp_pos = left;
        num_elements = (right - left + 1);

        while ((left <= left_end) && (mid <= right)) {
            if (numbers[left].compareTo(numbers[mid]) <= 0)
                temp[tmp_pos++] = numbers[left++];
            else
                temp[tmp_pos++] = numbers[mid++];
        }

        while (left <= left_end)
            temp[tmp_pos++] = numbers[left++];

        while (mid <= right)
            temp[tmp_pos++] = numbers[mid++];

        for (i = 0; i < num_elements; i++) {
            numbers[right] = temp[right];
            right--;
        }
    }

    private void sort(E[] numbers, int left, int right) {
        int mid;

        if (right > left) {
            mid = (right + left) / 2;
            sort(numbers, left, mid);
            sort(numbers, (mid + 1), right);

            DoMerge(numbers, left, (mid + 1), right);
        }
    }
}
