package com.getjavajob.training.algo07.hafizov.lesson06;

/**
 * Created by taala on 30.08.2015.
 */
public class AssociativeArrayTest {
    public static void main(String[] args) {
        AssociativeArray<String, String> hashMapCustom = new AssociativeArray<>();

        hashMapCustom.add(null,"GydZG_");
        hashMapCustom.add("polygenelubricants","polygenelubricants");
        hashMapCustom.add("DESIGNING","DESIGNING");

        hashMapCustom.display();
    }
}
