package com.getjavajob.training.algo07.hafizov.lesson06;

import com.getjavajob.training.algo07.util.Assert;

import java.util.TreeSet;

/**
 * Created by taala on 06.09.2015.
 */
public class PhonebookTest {
    static Phonebook phonebook = new Phonebook();
    public static void main(String[] args) {
        init();
        test();
    }

    public static void test(){
        Assert.assertEquals("800-515-5679", phonebook.search("Accor Hotels"));
        Assert.assertEquals("800-528-1238", phonebook.search("Best Western Hotels"));
        Assert.assertEquals("800-258-2633",phonebook.search("Club Med"));
    }

    public static void init(){
        phonebook.add("Accor Hotels","800-515-5679");
        phonebook.add("Aloft Hotels","877-462-5638");
        phonebook.add("AmericInn","800-634-3444");
        phonebook.add("Baymont","800-980-1679");
        phonebook.add("Best Western Hotels","800-528-1238");
        phonebook.add("Candlewood Suites","800-946-6200");
        phonebook.add("Choice Hotels","800-221-2222");
        phonebook.add("Club Med","800-258-2633");
        phonebook.add("Clarion Inns","800-252-7466");
        phonebook.add("Crown Plaza","800-227-6963");
    }
}
