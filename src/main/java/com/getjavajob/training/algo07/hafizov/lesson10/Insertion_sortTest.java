package com.getjavajob.training.algo07.hafizov.lesson10;

import com.getjavajob.training.algo07.util.Assert;

/**
 * Created by taala on 19.09.2015.
 */
public class Insertion_sortTest {
    public static void main(String[] args) {
        test();
    }

    public static void test() {
        Insertion_sort<String> stringInsertion_sort = new Insertion_sort<>();
        String[] testStr = new String[]{"b", "a", "d", "x", "c", "q", "m"};
        String[] resultStr = new String[]{"a", "b", "c", "d", "m", "q", "x"};
        Assert.assertEquals(resultStr, stringInsertion_sort.sort(testStr));

        Insertion_sort<Integer> integerInsertion_sort = new Insertion_sort<>();
        Integer[] testInt = new Integer[]{7, 4, 1, 8, 5, 2, 9, 6, 3};
        Integer[] resultInt = new Integer[]{1, 2, 3, 4, 5, 6, 7, 8, 9};
        Assert.assertEquals(resultInt, integerInsertion_sort.sort(testInt));
    }
}
