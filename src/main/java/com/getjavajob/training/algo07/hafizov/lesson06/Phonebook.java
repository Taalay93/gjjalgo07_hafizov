package com.getjavajob.training.algo07.hafizov.lesson06;

/**
 * Created by taala on 06.09.2015.
 */
public class Phonebook {
    AssociativeArray<String, String> book;
    public Phonebook(){
        book = new AssociativeArray<>();
    }
    public void add(String name, String number){
        book.add(name,number);
    }

    public String search(String name){
        String result = book.get(name);
        if(result== null){
            return null;
        }
        return result;
    }

    public void remove(String name){
        book.remove(name);
    }

}
