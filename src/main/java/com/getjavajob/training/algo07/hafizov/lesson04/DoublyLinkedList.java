package com.getjavajob.training.algo07.hafizov.lesson04;

import java.util.*;

/**
 * Created by taala on 20.08.2015.
 */
public class DoublyLinkedList<V>  extends AbstractList<V> implements java.util.List<V> {
    private int size;
    private Element<V> first;
    private Element<V> last;

    public boolean addFirst(V val) {
        Element e = new Element(null, null, val);
        if (first == null) {
            first = e;
            last = first;
        } else {
            first.prev = e;
            e.next = first;
            first = e;
        }
        size++;
        return true;
    }

    public boolean add(V val) {
        Element e = new Element(null, null, val);
        if (isEmpty()) {
            first = e;
            last = first;
        } else {
            e.prev = last;
            last.next = e;
            last = e;
        }
        size++;
        return true;
    }

    public void add(int i, V val) {
        Element e = new Element(null, null, val);
        if (i == 1) {
            addFirst(val);
            return;
        }
        if (i==size){
            add(val);
            return;
        }
        Element pr = first;
        for (int j = 2; j <= size; j++) {
            if (j == i) {
                Element temp = pr.next;
                pr.next = e;
                e.prev = pr;
                e.next = temp;
                temp.prev = e;
                break;
            }
            pr = pr.next;
        }
        size++;
    }

    public V remove(int index) {
        Element currentElement = first;
        Element previousElement = first;
        if (index == 1) {
            first = first.next;
            size--;
            return (V) first.val;
        }
        if (index==size){
            last = last.prev;
            size--;
            return (V) last.val;
        }
        for (int i = 2; i <= index; i++) {
            if (i == index) {
                previousElement.next = currentElement.next;
                size--;
                return (V) currentElement.val;
            }
            previousElement = currentElement;
            currentElement = currentElement.next;
        }
        return null;
    }

    @Override
    public int indexOf(Object o) {
        throw new UnsupportedOperationException();
    }

    @Override
    public int lastIndexOf(Object o) {
        throw new UnsupportedOperationException();
    }

    @Override
    public ListIterator<V> listIterator() {
        throw new UnsupportedOperationException();
    }

    @Override
    public ListIterator<V> listIterator(int index) {
        throw new UnsupportedOperationException();
    }

    @Override
    public List<V> subList(int fromIndex, int toIndex) {
        throw new UnsupportedOperationException();
    }

    public boolean remove(Object val) {
        if (first.val.equals(val)) {
            first = first.next;
            return true;
        } else {
            Element previousElement = first;
            for (Element currentElement = first; currentElement.val != null; previousElement = currentElement, currentElement = currentElement.next) {
                if (currentElement.val.equals(val)) {
                    previousElement.next = currentElement.next;
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean addAll(Collection<? extends V> c) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean addAll(int index, Collection<? extends V> c) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void clear() {
        throw new UnsupportedOperationException();
    }

    public void removeForIterator(int index) {
        throw new UnsupportedOperationException();
    }

    public void addForIterator(int i, V val) {
        add(i, val);
    }

    public void show() {
        if (size == 0) {
            return;
        }
        if (first.next == null) {
            System.out.println(first.val);
            return;
        }
        Element pr = first;

        System.out.println(first.val);
        pr = first.next;
        while (pr.next != null) {
            System.out.println(pr.val);
            pr = pr.next;
        }
        System.out.println(pr.val);
    }

    public ListIteratorImpl listIteratorImpl() {
        return new ListIteratorImpl();
    }

    public boolean isEmpty() {
        return first == null;
    }

    @Override
    public boolean contains(Object o) {
        return false;
    }

    @Override
    public Iterator<V> iterator() {
        return null;
    }

    @Override
    public Object[] toArray() {
        return new Object[0];
    }

    @Override
    public <T> T[] toArray(T[] a) {
        return null;
    }

    public int getSize() {
        return size;
    }

    @Override
    public V get(int index) {
        if (index == 1) {
            if (size == 1) {
                return first.val;
            }
            return first.val;
        }
        if (index == size) {
            return last.val;
        }
        Element pr = first.next;
        for (int i = 2; i <= size; i++) {
            if (i == index) {
                return (V) pr.val;
            }
            pr = pr.next;
        }
        return (V) pr.val;
    }

    @Override
    public V set(int index, V element) {
        return null;
    }

    @Override
    public int size() {
        return size;
    }

    class ListIteratorImpl implements java.util.ListIterator<V> {
        public ListIteratorImpl() {
            index = 1;
        }

        int index;

        @Override
        public boolean hasNext() {
            return index <= size;
        }

        @Override
        public V next() {
            return (V) get(index++);
        }

        @Override
        public boolean hasPrevious() {
            return index != 0;
        }

        @Override
        public V previous() {
            return (V) get(index--);
        }

        @Override
        public int nextIndex() {
            return index++;
        }

        @Override
        public int previousIndex() {
            return index--;
        }

        @Override
        public void remove() {
            removeForIterator(index);
        }

        @Override
        public void set(V o) {
            removeForIterator(index);
            addForIterator(index, o);
        }

        @Override
        public void add(V o) {
            addForIterator(index, o);
        }

        public void setIndex(int index) {
            if (index > 0 && index < size) {
                this.index = index;
            }
        }
    }
}
