package com.getjavajob.training.algo07.hafizov.lesson03;

/**
 * Created by admin on 15.08.2015.
 */
public class ListIteratorTest {
    private static DynamicArray<String> dinA;
    private static DynamicArray.ListIteratorImpl iter;
    private static String test = "test";

    public static void main(String[] args) {
        init();
        iter = dinA.listIterator();
        dinA.listIterator();
        nextTest();
        previousTest();
        removeTest();
        addTest();
    }

    public static void nextTest() {
        while (iter.hasNext()) {
            try {
                System.out.println(iter.next());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static void previousTest() {
        while (iter.hasPrevious()) {
            System.out.println(iter.previous());
        }
    }

    public static void removeTest() {
        while (iter.hasNext()) {
            try {
                iter.remove();
                iter.next();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static void addTest() {
        while (iter.hasNext()) {
            try {
                iter.add(test);
                iter.next();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static void init() {
        dinA = new DynamicArray<>();
        for (int i = 0; i < 15; i++) {
            dinA.add(test + i);
        }
    }
}
