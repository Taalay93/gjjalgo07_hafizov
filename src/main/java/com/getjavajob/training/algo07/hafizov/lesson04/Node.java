package com.getjavajob.training.algo07.hafizov.lesson04;

/**
 * Created by taala on 22.08.2015.
 */
public class Node<V> {
    public Node(V val){
        this.val = val;
    }
    Node<V> next;
    V val;
}
