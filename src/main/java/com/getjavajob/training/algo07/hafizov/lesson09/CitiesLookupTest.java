package com.getjavajob.training.algo07.hafizov.lesson09;

import com.getjavajob.training.algo07.util.Assert;

/**
 * Created by taala on 09.09.2015.
 */
public class CitiesLookupTest {
    private static CitiesLookup citiesLookup;

    public static void main(String[] args) {
        init();
        test();
    }

    public static void test(){
        Assert.assertEquals("Mosalsk Moscow ",citiesLookup.search("Mos"));
        Assert.assertEquals("Balakhna Balakovo Balashikha ",citiesLookup.search("Bal"));
        Assert.assertEquals("Kurbash Kurnetsk Kursk ",citiesLookup.search("Kur"));
    }

    public static void init() {
        citiesLookup = new CitiesLookup();
        citiesLookup.add("Moscow");
        citiesLookup.add("Mozdok");
        citiesLookup.add("Mosalsk");
        citiesLookup.add("Kursk");
        citiesLookup.add("Kurnetsk");
        citiesLookup.add("Kurbash");
        citiesLookup.add("Ladushkin");
        citiesLookup.add("Gudermes");
        citiesLookup.add("Gusev");
        citiesLookup.add("Uyar");
        citiesLookup.add("Uzhur");
        citiesLookup.add("Vysotsk");
        citiesLookup.add("Yalutorovsk");
        citiesLookup.add("Yakutsk");
        citiesLookup.add("Zverevo");
        citiesLookup.add("Asino");
        citiesLookup.add("Baksan");
        citiesLookup.add("Artyom");
        citiesLookup.add("Balakhna");
        citiesLookup.add("Balakovo");
        citiesLookup.add("Balashikha");
    }
}
