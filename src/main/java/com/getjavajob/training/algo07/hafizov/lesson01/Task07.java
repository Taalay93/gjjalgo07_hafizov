package com.getjavajob.training.algo07.hafizov.lesson01;

/**
 * Created by admin on 10.08.2015.
 */
public class Task07 {
    public static void main(String[] args) {
        System.out.println(swap2VarBitwise(5, 10));
    }

    public static String swap2VarBitwise(int x, int y) {
        x = x ^ y ^ (y = x);
        return "x= " + x + " y= " + y;
    }

    public static String swap2VarBitwise2(int x, int y) {
        x = x ^ y;
        y = x ^ y;
        x = x ^ y;
        return "x= " + x + " y= " + y;
    }

    public static String swap2VarArithmetic(int x, int y) {
        x = x + y;
        y = x - y;
        x = x - y;
        return "x= " + x + " y= " + y;
    }

    public static String swap2VarArithmetic2(int x, int y) {
        x = x * y;
        y = x / y;
        x = x / y;
        return "x= " + x + "y= " + y;
    }
}
