package com.getjavajob.training.algo07.hafizov.lesson10;

/**
 * Created by taala on 19.09.2015.
 */
public class Insertion_sort<E extends Comparable<E>> {
    public E[] sort(E[] arr) {
        int j;
        E key;
        int i;

        for (j = 1; j < arr.length; j++) {
            key = arr[j];
            for (i = j - 1; (i >= 0) && (arr[i].compareTo(key) > 0); i--) {
                arr[i + 1] = arr[i];
            }
            arr[i + 1] = key;
        }
        return arr;
    }
}
