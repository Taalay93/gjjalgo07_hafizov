package com.getjavajob.training.algo07.hafizov.lesson03;

import com.getjavajob.training.algo07.util.*;

import java.io.*;
import java.util.ArrayList;

/**
 * Created by admin on 12.08.2015.
 */
public class DynamicArrayPerformanceTest {
    private static ArrayList<String> list = new ArrayList<String>();
    private static int count = 500000;
    private static DynamicArray<String> dList = new DynamicArray<>();

    public static void main(String[] args) {
        String str = "test";
        String strDList = PerformanceTest.test(dList, count, str, "DynamicArrayList");
        String strList = PerformanceTest.test(list, count, str, "ArrayList");
        PerformanceTest.writeFile(strDList+strList,"./src/main/java/com/getjavajob/training/algo07/hafizov/lesson03/DynamicArrayList_VS_ArrayList.txt");
    }
}
