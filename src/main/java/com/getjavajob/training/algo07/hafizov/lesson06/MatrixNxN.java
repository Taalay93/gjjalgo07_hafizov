package com.getjavajob.training.algo07.hafizov.lesson06;

/**
 * Created by taala on 07.09.2015.
 */
public class MatrixNxN<V> implements Matrix<V> {
    private V[] arr;
    private int size;
    private AssociativeArray<Integer,V[]> map;
    public MatrixNxN(int size){
        this.size = size;
        map =new AssociativeArray<>();
        arr = (V[]) new Object[size];
    }

    public MatrixNxN(){
         this(100);
    }
    @Override
    public V get(int i, int j) {
        V[] temp = (V[]) map.get(i);
        return temp[j];
    }

    @Override
    public void set(int i, int j, V value) {
        if(i>=arr.length){
            throw new RuntimeException();
        }
        if(map.get(i)==null){
            V[] temp = (V[]) new Object[size];
            map.add(i,temp);
        }
        V[] temp = (V[]) map.get(i);
        temp[j] = value;
        map.add(i, temp);
    }
}
