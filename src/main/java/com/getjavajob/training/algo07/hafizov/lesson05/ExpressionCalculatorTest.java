package com.getjavajob.training.algo07.hafizov.lesson05;

import com.getjavajob.training.algo07.util.Assert;

/**
 * Created by taala on 24.08.2015.
 */
public class ExpressionCalculatorTest {
    public static void main(String[] args) {
        test();
    }

    public static void test(){
        String expression = "(1+2)*(3+4)/(12-5)";
        ExpressionCalculator convert = new ExpressionCalculator(expression);
        Assert.assertEquals("12+34+*125-/", convert.infixToPostfix());
    }
}
