package com.getjavajob.training.algo07.hafizov.lesson04;

import com.getjavajob.training.algo07.util.Assert;

/**
 * Created by taala on 23.08.2015.
 */
public class NodeReverseTest {
    public static void main(String[] args) {
        reverseTest();
    }

    public static void reverseTest(){
        LinkedList<String> list = new LinkedList<>();

        list.add("1");
        list.add("2");
        list.add("3");
        list.add("4");
        list.add("5");
        list.add("6");
        list.reverse();
        Assert.assertEquals("6 5 4 3 2 1 ",list.show());
    }
}
