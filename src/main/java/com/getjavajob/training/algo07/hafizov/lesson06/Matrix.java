package com.getjavajob.training.algo07.hafizov.lesson06;

/**
 * Created by taala on 07.09.2015.
 */
public interface Matrix<V> {
    V get(int i, int j);
    void set(int i, int j, V value);
}
