package com.getjavajob.training.algo07.hafizov.lesson07;

import com.getjavajob.training.algo07.util.Assert;

class ArrayBinaryTreeTest {
    public static void main(String[] args) {
        test();
    }

    public static void test() {
        ArrayBinaryTree<String> arrayBinaryTree = new ArrayBinaryTree<>();
        arrayBinaryTree.add("b", 1);
        arrayBinaryTree.add("a", 1);
        arrayBinaryTree.add("c", 1);
        arrayBinaryTree.add("x", 1);
        arrayBinaryTree.add("f", 1);

        Assert.assertEquals("a b c f x ", arrayBinaryTree.toString(1));

        arrayBinaryTree.remove("a", 1);

        Assert.assertEquals("b c f x ", arrayBinaryTree.toString(1));

        arrayBinaryTree.remove("f", 1);

        Assert.assertEquals("b c x ", arrayBinaryTree.toString(1));
    }
}