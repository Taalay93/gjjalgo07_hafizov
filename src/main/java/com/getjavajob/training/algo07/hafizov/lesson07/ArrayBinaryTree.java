package com.getjavajob.training.algo07.hafizov.lesson07;

import com.getjavajob.training.algo07.tree.*;

import java.util.Iterator;

/**
 * Concrete implementation of a binary tree using a node-based, linked structure
 *
 * @param <E> element
 */
public class ArrayBinaryTree<E extends Comparable<E>> {

    NodeImpl<E>[] root;

    ArrayBinaryTree() {
        root = new NodeImpl[100];
    }

    protected NodeImpl<E> validate(Node<E> n) throws IllegalArgumentException {
        try {
            return (NodeImpl<E>) n;
        } catch (Exception ex) {
            throw new IllegalArgumentException();
        }
    }

    public String toString(int index) {

        String result = "";

        if (root[index] != null) {
            result += toString(2 * index + 1);
            result += root[index].getElement() + " ";
            result += toString(2 * index);
        }
        return result;
    }


    public boolean add(E e, int index) throws IllegalArgumentException {
        if (index > root.length) {
            throw new IllegalArgumentException();
        } else {
            if (root[index] == null) {
                root[index] = new NodeImpl<>(e);
            } else {
                if (e.compareTo(root[index].value) > 0) {
                    add(e, index * 2);
                } else {
                    add(e, index * 2 + 1);
                }
            }
        }
        return true;
    }


    public boolean remove(E n, int index) throws IllegalArgumentException {
        if (root[index].getElement() == n) {
            root[index] = null;
            return true;
        }
        if (n.compareTo(root[index].value) > 0) {
            remove(n, index * 2);
        } else {
            remove(n, index * 2 + 1);
        }
        return false;
    }


    protected static class NodeImpl<E extends Comparable<E>> implements Node<E> {
        E value;

        public NodeImpl(E value) {
            this.value = value;
        }

        @Override
        public E getElement() {
            return value;
        }

        public int compareTo(E o) {
            return o.compareTo(value);
        }
    }
}
