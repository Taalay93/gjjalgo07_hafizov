package com.getjavajob.training.algo07.hafizov.lesson04;

import com.getjavajob.training.algo07.hafizov.lesson03.DynamicArray;
import com.getjavajob.training.algo07.util.PerformanceTest;

import java.util.ArrayList;
import java.util.LinkedList;

/**
 * Created by taala on 22.08.2015.
 */
public class JdkListsPerformanceTest {
    public static void main(String[] args) {
        ArrayList_vs_LinkedList();
        //DA_vs_DLL();
    }

    public static void ArrayList_vs_LinkedList(){
        ArrayList<String> arrlist = new ArrayList<>();
        LinkedList<String> linkedList = new LinkedList<>();
        String text = "test";
        int i =200000;
        String strArr = PerformanceTest.test(arrlist,i,text,"ArrayList");
        String strlinkedList = PerformanceTest.test(linkedList,i,text,"LinkedList");
        PerformanceTest.writeFile(strArr+strlinkedList,"./src/main/java/com/getjavajob/training/algo07/hafizov/lesson04/ArrayList_VS_LinkedList.txt");
    }

    public static void DA_vs_DLL(){
        DynamicArray<String> dynamicArray  =new DynamicArray<>();
        DoublyLinkedList<String> doublyLinkedList  = new DoublyLinkedList<>();
        String text = "test";
        int i =50000;
        String strDA = PerformanceTest.test(dynamicArray,i,text,"DynamicArray");
        String strDDL = PerformanceTest.test(doublyLinkedList,i,text,"DoublyLinkedList");
        PerformanceTest.writeFile(strDA+strDDL,"./src/main/java/com/getjavajob/training/algo07/hafizov/lesson04/DynamicArray_VS_DoublyLinkedList.txt");
    }
}
