package com.getjavajob.training.algo07.hafizov.lesson07;

import com.getjavajob.training.algo07.tree.*;
import com.getjavajob.training.algo07.tree.binary.*;

import java.util.*;


/**
 * Concrete implementation of a binary tree using a node-based, linked structure
 *
 * @param <E> element
 */
public class LinkedBinaryTree<E extends Comparable<E>> extends AbstractBinaryTree<E> {

    // nonpublic utility

    public void setRoot(NodeImpl<E> root) {
        this.root = root;
    }

    private NodeImpl<E> root;

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    int size;

    public LinkedBinaryTree() {
        size = 0;
    }

    /**
     * Validates the node is an instance of supported {@link NodeImpl} type and casts to it
     *
     * @param n node
     * @return casted {@link NodeImpl} node
     * @throws IllegalArgumentException
     */
    protected NodeImpl<E> validate(Node<E> n) throws IllegalArgumentException {
        try {
            return (NodeImpl<E>) n;
        } catch (Exception ex) {
            throw new IllegalArgumentException();
        }
    }

    public String toString(Node<E> in) {
        String result = "";
        NodeImpl<E> node = validate(in);
        if (node == null) {
            return "";
        }
        result += toString(node.getLeft());
        result += node.getElement() + " ";
        result += toString(node.getRight());


        return result;
    }
    // update methods supported by this class

    @Override
    public Node<E> addRoot(E e) throws IllegalStateException {
        root.setElement(e);
        return root;
    }

    public void add(E value) {
        add(root, value);
        size++;
    }

    @Override
    public Node<E> add(Node<E> n, E e) throws IllegalArgumentException {
        NodeImpl<E> el = validate(n);
        if (root == null) {
            root = new NodeImpl<E>(e, null, null);
            root.setParent(null);
            return root;
        }
        if (el.getElement().compareTo(e) > 0) {
            if (el.getLeft() != null) {
                add(el.getLeft(), e);
            } else {
                el.left = new NodeImpl<E>(e, null, null);
                el.left.setParent(el);
            }
        } else {
            if (el.getRight() != null) {
                add(el.right, e);
            } else {
                el.right = new NodeImpl<E>(e, null, null);
                el.right.setParent(el);
            }
        }
        return el;
    }

    @Override
    public Node<E> addLeft(Node<E> n, E e) throws IllegalArgumentException {
        NodeImpl<E> el = validate(n);
        if (el.getElement().compareTo(e) > 0) {
            if (el.getLeft() != null) {
                addLeft(el.getLeft(), e);
            } else {
                el.left = new NodeImpl<E>(e, null, null);
            }
        }
        return el;
    }

    @Override
    public Node<E> addRight(Node<E> n, E e) throws IllegalArgumentException {
        NodeImpl<E> el = validate(n);

        if (el.getRight() != null) {
            addRight(el.right, e);
        } else {
            el.right = new NodeImpl<E>(e, null, null);
        }

        return el;
    }

    /**
     * Replaces the element at {@link Node} <i>n</i> with <i>e</i>
     *
     * @param n node
     * @param e element
     * @return replace element
     * @throws IllegalArgumentException
     */
    @Override
    public E set(Node<E> n, E e) throws IllegalArgumentException {
        NodeImpl<E> node = validate(n);
        E element = node.getElement();
        node.setElement(e);
        return element;
    }

    @Override
    public E remove(Node<E> n) throws IllegalArgumentException {
        return null;
    }

    /**
     * Replaces the element at {@link Node} <i>n</i> with <i>e</i>
     *
     * @return replace element
     * @throws IllegalArgumentException
     */

    public boolean remove(E key) {
        NodeImpl<E> focusNode = root;
        NodeImpl<E> parent = root;
        boolean isItALeftChild = true;
        while (focusNode.getElement() != key) {
            parent = focusNode;
            if (key.compareTo(focusNode.getElement()) < 0) {//focusNode.getElement().compareTo(key) > 0
                isItALeftChild = true;
                focusNode = focusNode.getLeft();
            } else {
                isItALeftChild = false;
                focusNode = focusNode.getRight();
            }
            if (focusNode == null) {
                return false;
            }
        }
        if (focusNode.getLeft() == null && focusNode.getRight() == null) {
            if (focusNode == root) {
                root = null;
            } else if (isItALeftChild) {
                parent.left = null;
            } else {
                parent.right = null;
            }
        } else if (focusNode.getRight() == null) {
            if (focusNode == root) {
                root = focusNode.getLeft();
            } else if (isItALeftChild) {
                parent.left = focusNode.getLeft();
            } else {
                parent.right = focusNode.getLeft();
            }
        } else if (focusNode.getLeft() == null) {
            if (focusNode == root) {
                root = focusNode.getRight();
            } else if (isItALeftChild) {
                parent.left = focusNode.right;
            } else {
                parent.right = focusNode.getLeft();
            }
        } else {
            NodeImpl<E> replacement = getReplacementNode(focusNode);
            if (focusNode == root) {
                root = replacement;
            } else if (isItALeftChild) {
                parent.left = replacement;
            } else {
                parent.right = replacement;
            }
            replacement.left = focusNode.left;
        }
        size--;
        return true;
    }

    private NodeImpl<E> getReplacementNode(NodeImpl<E> replaceNode) {
        NodeImpl<E> replacementParent = replaceNode;
        NodeImpl<E> replacement = replaceNode;

        NodeImpl<E> focusNode = replaceNode.getRight();

        while (focusNode != null) {
            replacementParent = replacement;
            replacement = focusNode;
            focusNode = focusNode.left;
        }
        if (replacement != replacement.right) {
            replacementParent.left = replacement.right;
            replacement.right = replaceNode.right;
        }
        return replacement;

    }

    public void removeForIterator(E key){
        remove(key);
    }

    // {@link Tree} and {@link BinaryTree} implementations

    @Override
    public Node<E> left(Node<E> p) throws IllegalArgumentException {
        return validate(p).getLeft();
    }

    @Override
    public Node<E> right(Node<E> p) throws IllegalArgumentException {
        return validate(p).getRight();
    }

    @Override
    public Node<E> root() {
        return root;
    }

    @Override
    public Node<E> parent(Node<E> n) throws IllegalArgumentException {
        return validate(n).parent;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public Iterator<E> iterator() {
        return new TreeIterator();
    }

    @Override
    public Iterable<Node<E>> nodes() {
        return null;
    }

    class TreeIterator implements Iterator<E> {

        private Stack<NodeImpl> stack = new Stack<>();
        private NodeImpl<E> current;
        TreeIterator() {
            current = (NodeImpl<E>) root();
        }

        @Override
        public boolean hasNext() {
            return (!stack.isEmpty() || current != null);
        }

        @Override
        public E next() {
            while (current != null) {
                stack.push(current);
                current = current.left;
            }

            current = stack.pop();
            NodeImpl<E> node = current;
            current = current.right;
            return node.getElement();
        }

        @Override
        public void remove() {
            removeForIterator(current.getElement());
        }
    }

    protected static class NodeImpl<E extends Comparable<E>> implements Node<E> {
        public NodeImpl(E value, NodeImpl<E> left, NodeImpl<E> right) {
            this.element = value;
            this.left = left;
            this.right = right;
            this.parent = null;
            height = 0;
        }

        public NodeImpl() {
            this(null, null, null);
        }

        private NodeImpl<E> left;
        private NodeImpl<E> right;

        public NodeImpl<E> getParent() {
            return parent;
        }

        public void setParent(NodeImpl<E> parent) {
            this.parent = parent;
        }

        private NodeImpl<E> parent;
        private E element;
        private int height;

        public int getHeight() {
            return height;
        }

        public void setHeight(int height) {
            this.height = height;
        }

        public NodeImpl<E> getRight() {
            return right;
        }

        public int compareTo(E o) {
            return o.compareTo(element);
        }

        public void setRight(NodeImpl<E> right) {
            this.right = right;
        }

        public NodeImpl<E> getLeft() {
            return left;
        }

        public void setLeft(NodeImpl<E> left) {
            this.left = left;
        }

        public void setElement(E value) {
            this.element = value;
        }

        @Override
        public E getElement() {
            return element;
        }
    }

}