package com.getjavajob.training.algo07.hafizov.lesson06;

import com.getjavajob.training.algo07.util.Assert;

/**
 * Created by taala on 07.09.2015.
 */
public class MatrixTest {
    public static void main(String[] args) {
        test();
    }

    public static void test(){
        MatrixNxN<Integer> init = new MatrixNxN<>(1000001);
        for (int i = 0; i <= 1000000 ; i++) {
            init.set(1000000, i, i);
        }

        Assert.assertEquals(1000000, init.get(1000000,1000000));
    }
}
