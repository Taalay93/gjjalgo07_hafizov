package com.getjavajob.training.algo07.hafizov.lesson05;

/**
 * Created by taala on 23.08.2015.
 */
public class test {
    public static void main(String[] args) {
        LinkedListQueue<String> str = new LinkedListQueue<>();

        str.add("1");
        str.add("2");
        str.add("3");
        str.add("4");
        str.add("5");
        str.add("6");
        str.add("7");
        str.add("8");
        str.showAll();
        System.out.println();
        str.remove();
        str.showAll();
    }
}
