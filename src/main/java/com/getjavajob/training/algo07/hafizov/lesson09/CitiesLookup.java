package com.getjavajob.training.algo07.hafizov.lesson09;

import com.getjavajob.training.algo07.tree.Node;
import com.getjavajob.training.algo07.tree.binary.search.BinarySearchTree;

/**
 * Created by taala on 09.09.2015.
 */
public class CitiesLookup extends BinarySearchTree<String> {
    public static void main(String[] args) {

    }

    public String search(String searchName) {
        Node<String> tepm = treeSearch(root(), searchName);
        if (tepm == null) {
            return null;
        }
        return toString(tepm,searchName);
    }

    public String toString(Node<String> in, String str) {
        String result = "";
        NodeImpl<String> node = validate(in);
        if (node != null && node.getElement().startsWith(str)) {
            result += toString(node.getLeft(),str);
            result += node.getElement() + " ";
            result += toString(node.getRight(),str);
        }
        return result;
    }

    @Override
    public Node<String> treeSearch(Node<String> n, String val) {
        NodeImpl<String> node = validate(n);
        if (node != null) {
            if (node.getElement().startsWith(val)) {
                return node;
            } else {
                if (node.getElement().compareTo(val) > 0) {
                    return treeSearch(node.getLeft(), val);
                } else {
                    return treeSearch(node.getRight(), val);
                }
            }
        } else {
            return null;
        }
    }

    public void tepm(String name) {
        String result = "";
        for (String n1 : this) {
            if (n1.toLowerCase().contains(name.toLowerCase())) {
                result += n1 + " ";
            }
        }
        //return new StringBuilder(result).deleteCharAt(result.length()-1).toString();
    }
}
