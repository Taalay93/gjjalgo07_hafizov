package com.getjavajob.training.algo07.hafizov.lesson04;

import com.getjavajob.training.algo07.util.*;

import java.util.LinkedList;

/**
 * Created by taala on 21.08.2015.
 */
public class DoublyLinkedPerformanceTest {
    private static DoublyLinkedList<String> dl;
    private static LinkedList<String> ll;
    private static String txt = "test";
    private static int doForNTimes = 1000000;

    public static void main(String[] args) {
        dl = new DoublyLinkedList<>();
        ll = new LinkedList<>();
        String strDL = PerformanceTest.test(dl, doForNTimes, txt, "DoublyLinkedList");
        String strLL = PerformanceTest.test(ll, doForNTimes, txt, "LinkedList");
        PerformanceTest.writeFile(strDL + strLL, "./src/main/java/com/getjavajob/training/algo07/hafizov/lesson04/DoublyLinkedList_VS_LinkedList.txt");
    }

}