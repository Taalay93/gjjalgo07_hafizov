package com.getjavajob.training.algo07.hafizov.lesson05;
import com.getjavajob.training.algo07.hafizov.lesson04.*;
/**
 * Created by taala on 23.08.2015.
 */
public class LinkedListStack<V> implements Stack<V> {

    LinkedList<V> list = new LinkedList<>();

    @Override
    public void push(V v) {
        list.insertFirst(v);
    }

    @Override
    public V pop() {
            return list.deleteFirst();
    }

    public String displayStack() {
        return list.show();
    }
}
