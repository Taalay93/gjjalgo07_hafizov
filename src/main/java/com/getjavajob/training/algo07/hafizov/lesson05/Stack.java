package com.getjavajob.training.algo07.hafizov.lesson05;

/**
 * Created by taala on 23.08.2015.
 */
public interface Stack<E> {
    void push(E e); // add element to the top
    E pop(); // removes element from the top
}
