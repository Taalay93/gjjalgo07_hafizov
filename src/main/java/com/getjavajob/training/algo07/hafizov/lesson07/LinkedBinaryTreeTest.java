package com.getjavajob.training.algo07.hafizov.lesson07;

import com.getjavajob.training.algo07.util.Assert;

import java.util.Iterator;

class LinkedBinaryTreeTest {
    public static void main(String[] args) {
        test();
    }

    public static void test() {
        LinkedBinaryTree<String> linkedBinaryTree = new LinkedBinaryTree<>();

        linkedBinaryTree.add("c");
        linkedBinaryTree.add("a");
        linkedBinaryTree.add("e");
        linkedBinaryTree.add("d");
        linkedBinaryTree.add("f");
        linkedBinaryTree.add("x");
        Assert.assertEquals("a c d e f x ", linkedBinaryTree.toString(linkedBinaryTree.root()));

        linkedBinaryTree.remove("c");

        Assert.assertEquals("a d e f x ", linkedBinaryTree.toString(linkedBinaryTree.root()));

        linkedBinaryTree.remove("a");

        Assert.assertEquals("d e f x ", linkedBinaryTree.toString(linkedBinaryTree.root()));

        Iterator<String> stringIterator = linkedBinaryTree.iterator();

        String test = "";
        while (stringIterator.hasNext()){
            test += stringIterator.next()+" ";
        }

        Assert.assertEquals("d e f x ", test);
    }
}