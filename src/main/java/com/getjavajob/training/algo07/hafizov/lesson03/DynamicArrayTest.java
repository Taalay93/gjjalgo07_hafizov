package com.getjavajob.training.algo07.hafizov.lesson03;

import com.getjavajob.training.algo07.util.Assert;

/**
 * Created by admin on 12.08.2015.
 */
public class DynamicArrayTest extends Throwable {
    private static DynamicArray<String> dList;
    private static String test = "test";

    public static void main(String[] args) {
        dList = new DynamicArray<>();
        testAddStart();
        testAddMiddle();
        testAddEnd();

        testRemoveStart();
        testRemoveMiddle();
        testRemoveEnd();
    }

    public static void testAddStart() {
        dList.add(test);
        dList.add(test);
        dList.add(test);
        dList.add(test);
        Assert.assertEquals("test", dList.get(3));
    }

    public static void testAddMiddle() {
        dList.add(4, test);
        dList.add(5, test);
        dList.add(6, test);
        dList.add(7, test);
        Assert.assertEquals("test", dList.get(7));
    }

    public static void testAddEnd() {
        dList.add(8, test);
        dList.add(9, test);
        dList.add(10, test);
        dList.add(11, test);
        Assert.assertEquals("test", dList.get(11));
    }

    public static void testRemoveStart() {
        dList.remove(0);
        dList.remove(1);
        Assert.assertEquals("test", dList.get(3));
    }

    public static void testRemoveMiddle() {
        dList.remove(2);
        dList.remove(3);
        Assert.assertEquals("test", dList.get(5));
    }

    public static void testRemoveEnd() {
        dList.remove(4);
        dList.remove(5);
        Assert.assertEquals("test", dList.get(5));
    }
}

