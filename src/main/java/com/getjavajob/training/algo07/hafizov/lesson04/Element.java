package com.getjavajob.training.algo07.hafizov.lesson04;

/**
 * Created by taala on 20.08.2015.
 */
public class Element<V> {
    public Element(Element<V> prev, Element<V> next, V val) {
        this.prev = prev;
        this.next = next;
        this.val = val;
    }

    Element<V> prev;
    Element<V> next;
    V val;
}
