package com.getjavajob.training.algo07.hafizov.lesson05;

import com.getjavajob.training.algo07.util.Assert;

/**
 * Created by taala on 23.08.2015.
 */
public class LinkedListQueueTest {

    public static void main(String[] args) {
        test();
    }
    public static void test(){
        LinkedListQueue<String> str = new LinkedListQueue<>();

        str.add("1");
        str.add("2");
        str.add("3");
        str.add("4");
        str.add("5");
        str.add("6");
        str.add("7");
        str.add("8");
        Assert.assertEquals("8 7 6 5 4 3 2 1 ", str.showAll());

        str.remove();

        Assert.assertEquals("8 7 6 5 4 3 2 ", str.showAll());
    }
}
