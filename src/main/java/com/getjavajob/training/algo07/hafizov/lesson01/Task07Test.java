package com.getjavajob.training.algo07.hafizov.lesson01;

import com.getjavajob.training.algo07.util.Assert;
import com.sun.javafx.tk.Toolkit;

/**
 * Created by admin on 12.08.2015.
 */
public class Task07Test {
    public static void main(String[] args) {
        testSwap2VarArithmetic();
        testSwap2VarArithmetic2();
        testSwap2VarBitwise();
        testSwap2VarBitwise2();
    }

    public static void testSwap2VarBitwise(){
        Assert.assertEquals("x= 7 y= 10", Task07.swap2VarArithmetic(10,7));
    }

    public static void testSwap2VarBitwise2(){
        Assert.assertEquals("x= 7 y= 10", Task07.swap2VarArithmetic(10,7));
    }

    public static void testSwap2VarArithmetic(){
        Assert.assertEquals("x= 7 y= 10", Task07.swap2VarArithmetic(10,7));
    }

    public static void testSwap2VarArithmetic2(){
        Assert.assertEquals("x= 7 y= 10", Task07.swap2VarArithmetic(10,7));
    }
}
