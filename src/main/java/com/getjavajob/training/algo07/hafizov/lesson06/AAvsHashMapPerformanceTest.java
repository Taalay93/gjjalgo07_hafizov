package com.getjavajob.training.algo07.hafizov.lesson06;

import com.getjavajob.training.algo07.util.PerformanceTest;

import java.util.HashMap;


/**
 * Created by taala on 04.09.2015.
 */
public class AAvsHashMapPerformanceTest {
    public static void main(String[] args) {
        test();
    }

    public static void test(){
        HashMap<String,String> hashMap = new HashMap<>();
        AssociativeArray<String,String> associativeArray = new AssociativeArray<>();
        String text = "test";
        int i =50000;
        String strAssociativeArray = PerformanceTest.test(associativeArray,i,text,"associativeArray");
        String strMap = PerformanceTest.test(hashMap, i, text, "hashMap");
        PerformanceTest.writeFile(strMap+"\n"+strAssociativeArray,"./src/main/java/com/getjavajob/training/algo07/hafizov/lesson06/HashMap_VS_AssociativeArray.txt");
    }

}
