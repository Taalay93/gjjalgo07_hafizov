package com.getjavajob.training.algo07.hafizov.lesson03;

import java.util.*;

/**
 * Created by admin on 12.08.2015.
 */
public class DynamicArray<T> extends AbstractList<T> implements List<T> {
    public DynamicArray(int size) {
        this.sizeObj = size;
        obj = (T[]) new Object[sizeObj];
        this.size = 0;
        ArrayList<String> a = new ArrayList<>();
    }

    public DynamicArray() {
        this(10);
    }

    private int sizeObj = 10;
    int size;
    private Collection c;
    private T[] obj;

    public boolean add(T e) {
        if (size >= sizeObj) {
            obj = growArray(obj);
        }
        try {
            obj[size++] = e;
            return true;
        } catch (Exception ex) {
            return false;
        }
    }

    public void add(int i, T e) {
        if (size >= sizeObj) {
            obj = growArray(obj);
        }
        T[] temp = (T[]) new Object[obj.length];
        for (int j = 0; j < size + 1; j++) {
            if (i == j) {
                temp[j] = e;
            } else {
                temp[j] = obj[j > i ? j - 1 : j];
            }
        }
        obj = temp;
        size++;
    }

    public T set(int i, T e) {
        T temp = obj[i];
        obj[i] = e;
        return temp;
    }

    public T get(int i) {
        return obj[i];
    }

    public T remove(int i) {
        if (size >= sizeObj) {
            obj = growArray(obj);
        }
        T temp = obj[i];
        int numMoved = size - i - 1;
        System.arraycopy(obj, i + 1, obj, i, numMoved);
        obj[--size] = null;
        //size--;
        return temp;
    }

    public boolean remove(Object e) {
        try {
            for (int j = 0; j < size; j++) {
                if (e.equals(obj[j])) {
                    obj[j] = obj[j + 1];
                } else {
                    obj[j] = obj[j];
                }
            }
            size--;
            return true;
        } catch (Exception ex) {
            return false;
        }
    }

    public int indexOf(Object e) {
        for (int j = 0; j < size; j++) {
            if (e.equals(obj[j])) {
                return j;
            }
        }
        return -1;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    public boolean contains(Object e) {
        for (int j = 0; j < size; j++) {
            if (e.equals(obj[j])) {
                return true;
            }
        }
        return false;
    }

    @Override
    public T[] toArray(Object[] a) {
        return obj;
    }

    private T[] growArray(T[] e) {
        sizeObj = (sizeObj * 3) / 2 + 1;
        T[] temp = (T[]) new Object[sizeObj];
        System.arraycopy(e, 0, temp, 0, size);
        return temp;
    }

    public ListIteratorImpl<T> listIterator() {
        return new ListIteratorImpl<T>();
    }

    public int getSize() {
        return obj.length;
    }

    @Override
    public int lastIndexOf(Object o) {
        throw new UnsupportedOperationException();
    }
    @Override
    public ListIterator<T> listIterator(int index) {
        throw new UnsupportedOperationException();
    }

    @Override
    public List<T> subList(int fromIndex, int toIndex) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean addAll(int index, Collection<? extends T> c) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        throw new UnsupportedOperationException();
    }

    public int getIndex() {
        return size;
    }

    class ListIteratorImpl<T> implements java.util.ListIterator<T> {
        public ListIteratorImpl() {
            iterObj = (T[]) new Object[size];
            indexI = 0;
            sizeI = getIndex();
            init();
        }

        T[] iterObj;
        int indexI;
        int sizeI;

        public boolean hasNext() {
            return indexI < sizeI ? true : false;
        }

        public T next() {
            try {
                return iterObj[indexI++];
            } catch (Exception ex) {
                throw new NoSuchElementException();
            }
        }

        public boolean hasPrevious() {
            try {
                return indexI > 0 ? true : false;
            } catch (Exception ex) {
                throw new NoSuchElementException();
            }
        }

        public T previous() {
            return iterObj[--indexI];
        }

        public int nextIndex() {
            return indexI++;
        }

        public int previousIndex() {
            return indexI--;
        }

        public void remove() {
            if (indexI == -1 || indexI > sizeI) {
                throw new IllegalStateException();
            }
            iterObj[indexI] = null;
        }

        public void set(T e) {
            iterObj[indexI] = e;
        }

        void init() {
            for (int i = 0; i < sizeI; i++) {
                iterObj[i] = (T) obj[i];
            }
        }

        public void add(T e) {
            if (indexI >= sizeI) {
                growArray(iterObj);
            }
            iterObj[indexI] = e;
            indexI++;
        }

        T[] growArray(Object[] e) {
            sizeI = (sizeI * 3) / 2 + 1;
            T[] temp = (T[]) new Object[sizeI];
            System.arraycopy(e, 0, temp, 0, size);
            return temp;
        }
    }
}
